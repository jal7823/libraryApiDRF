# :book: Library API

---
API for manager books in different branch libraries
## Technologies used

* Server
    * Django
    * Django Rest Framework
* Data Base
    * MySQL
        * ORM Django
* Documentation
    * drf-yasg
    *spectacular

## :gear: Installation and configuration

### Environment

:warning: Is very important before install any package create an environment, you can use **virtualenv** or **anaconda**

##### Anaconda
```
conda activate nameofyourenvironent
```

##### virtualenv (Linux) 
```
source nameofyourenvironment/bin/activate
```

##### virtualenv (Windows) 
```
.\nameofyourenvironment\Script|activate
```
### clone this repositori
```
git clone https://github.com/Jal7823/LibraryApiDRF.git
```

### :gear: Installed packages

All packages will be installed running the next command line

```
pip3 install -r requirements.txt
```

### :gear: Configure connection to Data Base

Create a new file **.env** and set the following variables:

```
'NAME': 'name db',
'USER': 'user db',
'PASSWORD': 'password db',
'HOST': 'localhost or if you want to connect to a different host name got here',
'PORT': '3306',
```

### Run project

Now just need run :

```
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py runserver
```

Now you can open some of these links:  
* [swagger]('http://localhost:8000/')   
* [redoc]('http://localhost:8000/redoc') 


### Test API user

This project is in build mode, you can test the API with the user:

username:***test*** <br>
email:***test@test.com*** <br>
password:***test***<br>

### Usage

You need get a token, for use any endpoint, so you can use the end point ***TOKEN*** ,you should be send the user and password provided in ***Test API user***, this return a token, use it to authorization and this give you access to use any endpoint

## Coming soon ... 

Quick installation with Docker