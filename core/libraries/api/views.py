from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated,IsAdminUser

from ..models import Library
from .serializer import SerializerLibrary



class LibraryViewSets(viewsets.ModelViewSet):
    queryset = Library.objects.all()
    serializer_class = SerializerLibrary
    permission_classes = [IsAdminUser]