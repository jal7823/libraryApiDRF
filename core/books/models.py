from django.db import models
from ..author.models import Author
from ..category.models import Category
from ..editorial.models import Editorial



class Books(models.Model):
    name = models.CharField('Nombre',max_length=50)
    author = models.ForeignKey(Author,on_delete=models.CASCADE,related_name='Autor')
    price = models.DecimalField('Precio',max_digits=10, decimal_places=2)
    category = models.ForeignKey(Category,on_delete=models.CASCADE,related_name='Categoria',null=True,blank=True)
    editorial = models.ForeignKey(Editorial,on_delete=models.CASCADE,related_name='Editorial',null=True,blank=True)
    image = models.ImageField(upload_to='frontPage')
    publication = models.IntegerField(default=1800)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'Books'
        verbose_name = 'Book'
        verbose_name_plural = 'Books'
    