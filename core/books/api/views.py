from rest_framework import viewsets,status
from rest_framework.response import Response
from rest_framework.schemas import AutoSchema
from rest_framework.permissions import IsAuthenticated
from drf_yasg.utils import swagger_auto_schema

from ..models import Books
from .serializer import SerializerBooks

@swagger_auto_schema(tags=['books'])
class BooksViewSets(viewsets.ModelViewSet): 
    permission_classes = [IsAuthenticated]
    queryset = Books.objects.all()
    serializer_class = SerializerBooks


