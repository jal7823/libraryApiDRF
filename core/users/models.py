from django.db import models
from django.contrib.auth.models import AbstractUser,UserManager


class UserManager(UserManager):
    def create_user(self,username,email,password=None):
        if not email:
            raise ValueError('Users must have an email address')
        else:
            user = self. model(
                username = username,
                email = self.normalize_email(email),
            )
            user.set_password(password)
            user.save(using=self._db)
            return user
    
    def create_superuser(self,username,email,password):
        user = self. create_user(username,email,password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

class Users(AbstractUser):
    username = models.CharField('Username', max_length=100, unique=True)
    name = models.CharField('Name', max_length=100)
    email = models.EmailField('Email', max_length=100, unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    objects = UserManager()

    USER_NAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email',]


    def __str__(self):
        return self.username

    def has_perm(self,perm,obj=None):
        return True

    def has_module_perms(self,app_label):
        return True
    

