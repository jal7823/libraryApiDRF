from rest_framework import viewsets,status

from ..models import Category
from .serializer import CategorySerializers
from rest_framework.permissions import IsAuthenticated



class CategoryViewSets(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializers  
    permission_classes = [IsAuthenticated]