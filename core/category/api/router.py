from django.urls import path,include
from rest_framework.routers import DefaultRouter
from .views import CategoryViewSets


router = DefaultRouter()

router.register(r'',CategoryViewSets,basename='category')

urlpatterns =[
    path('',include(router.urls)),
]