from django.db import models

class Editorial(models.Model):
    name = models.CharField(max_length=50)
    
    def __str__(self):
        return self.name

    class Meta:
        db_table = 'Editorial'
        verbose_name = 'Editorial'
        verbose_name_plural = 'Editorials'