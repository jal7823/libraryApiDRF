from rest_framework import serializers
from ..models import Editorial

class SerializerEditorial(serializers.ModelSerializer):
    class Meta:
        model = Editorial
        fields = '__all__'