from django.urls import path,include
from rest_framework.routers import DefaultRouter

from .views import EditorialViewSets

router = DefaultRouter()

router.register(r'',EditorialViewSets,basename='editorial')


urlpatterns = [
    path('', include(router.urls)),
]