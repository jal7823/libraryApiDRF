from rest_framework import viewsets,status

from ..models import Editorial
from .serializer import SerializerEditorial
from rest_framework.permissions import IsAuthenticated


class EditorialViewSets(viewsets.ModelViewSet):
    queryset = Editorial.objects.all()
    serializer_class = SerializerEditorial
    permission_classes = [IsAuthenticated]