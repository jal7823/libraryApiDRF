from rest_framework import serializers
from ..models import Author


class SerializerAuthor(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = '__all__'
        
    def validate(self,data):
        print('===============>',data['name'])
        return data

