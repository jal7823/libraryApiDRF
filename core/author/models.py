from django.db import models


class Author(models.Model):
    name = models.CharField(max_length=50)
    
    def __str__(self):
        return self.name

    class Meta:
        db_table = 'Author'
        verbose_name = 'Author'
        verbose_name_plural = 'Authors'
